<?php

class Presupuesto extends \Eloquent {
	protected $fillable = ['folio','paquete','cliente','precio','is_active','closed_at','posted_by','comentarios'];
	protected $table = "presupuestos";

	public function fechas()
	{
		return $this->hasOne('PresupuestoFechas','id_presupuesto','id');
	}

	public function clientes()
	{
		//return $this->hasOne('PresupuestoClientes','id_presupuesto','id');
		return $this->hasOne('Cliente','id','idCliente');
	}

	public function items()
	{
		return $this->hasMany('PresupuestoArticulos','id_presupuesto','id');
	}

	public function pagos()
	{
		return $this->hasMany('Pago','id_presupuesto','id');
	}
}
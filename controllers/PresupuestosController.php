<?php

class PresupuestosController extends \BaseController {

	protected $layout = 'layout.default';

	/**
	 * Display a listing of the resource.
	 * GET /presupuestos
	 *
	 * @return Response
	 */
	public function index()
	{
		$presupuestos = Presupuesto::orderBy('id','desc')->get();

		$view = View::make('presupuestos.index')
					->with('presupuestos',$presupuestos);

		$this->layout->title   = "Presupuestos | Salón la Cúpula";
		$this->layout->menu    = 2;
		$this->layout->content = $view;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /presupuestos/create
	 *
	 * @return Response
	 */
	public function create()
	{

		$clientes   = array(""=>"&mdash; Seleccione Opción &mdash;") + Cliente::orderBy('nombre','asc')->lists('nombre','id');
		$paquetes   = array(""=>"&mdash; Seleccione Opción &mdash;") + Paquete::orderBy('clave','asc')->lists('clave','id');

		$categorias = array(""=>"&mdash; Seleccione Opción &mdash;")+Categoria::orderBy('nombre','asc')->lists('nombre','id');
		$view = View::make('presupuestos.create')
					->with('categorias',$categorias)
					->with('paquetes',$paquetes)
					->with('clientes',$clientes);

		$this->layout->title   = "Presupuestos | Salón la Cúpula";
		$this->layout->menu    = 2;
		$this->layout->content = $view;
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /presupuestos
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = array(
					'idCliente'     => Input::get('idCliente'),
					'nombrePaquete' => Input::get('nombrePaquete'),
					'salon'         => Input::get('salon'),
					'fecha_evento'  => Input::get('fecha_evento'),
					'hora_evento'   => Input::get('hora_evento'),
					'fecha_termina' => Input::get('fecha_termina'),
					'hora_termina'  => Input::get('hora_termina')
					);
		$rules = array(
					'idCliente'     => 'required',
					'nombrePaquete' => 'required',
					'salon'         => 'required',
					'fecha_evento'  => 'required',
					'hora_evento'   => 'required',
					'fecha_termina' => 'required',
					'hora_termina'  => 'required'
					);

		$messages = array(
						'idCliente.required'     =>'El campo Nombre Cliente es requerido.',
						'nombrePaquete.required' =>'El campo Nombre del Paquete es requerido.',
						'salon.required'         =>'El campo :attribute es requerido.',
						'fecha_evento.required'  =>'El campo Fecha Evento es requerido.',
						'hora_evento.required'   =>'El campo Hora Evento es requerido.',
						'fecha_termina.required' =>'El campo Fecha Termina es requerido.',
						'hora_termina.required'  =>'El campo Hora Termina es requerido.'
						);

		$validator = Validator::make($data,$rules,$messages);

		if ($validator->fails()) {

			return Redirect::to('presupuestos/create')->withErrors($validator)->withInput();
		}else{

			$pre = Presupuesto::orderBy('id','desc')->first();
			$thisYear   = date('y');
			$cadena     = "";

			if(count($pre) > 0 ){
				$year = substr($pre->folio, 3,2);
				$cont = substr($pre->folio, 6,3);

				if ($year == $thisYear ){
					$cont = $cont+1;
					switch (strlen($cont)) {
						case '1':
							$cadena = "00";
							break;
						case '2':
							$cadena = "0";
							break;
					}
					$newFolio = "PO-".$thisYear."-".$cadena.$cont;
				}else{
					$newFolio = "PO-".$thisYear."-001";
				}

			}else{
				$newFolio = "PO-".$thisYear."-001";
			}

			$cliente = Cliente::find(Input::get('idCliente'));
			$paquete = Paquete::find(Input::get('idPaquete'));

			$presupuesto = New Presupuesto;
			$presupuesto->folio     = $newFolio;
			// $presupuesto->paquete   = $paquete->evento."-".$paquete->cantidad_personas."-".$paquete->numero;
			$presupuesto->paquete   = Input::get('nombrePaquete');
			$presupuesto->idCliente = $cliente->id;
			$presupuesto->cliente   = $cliente->nombre;
			$presupuesto->posted_by = Session::get('username');
			$presupuesto->save();


			$ID = DB::getPdo()->lastInsertId();

			$clien = New PresupuestoClientes;
			$clien->id_presupuesto = $ID;
			$clien->id_cliente = $cliente->id;
			$clien->nombre     = $cliente->nombre;
			$clien->direccion  = $cliente->direccion." ".$cliente->colonia." ".$cliente->ciudad;
			$clien->telefono   = $cliente->telefono;
			$clien->email      = $cliente->mail;
			$clien->fec_nac    = $cliente->fec_nac;
			$clien->save();

			$fecha = New PresupuestoFechas;
			$fecha->id_presupuesto = $ID;
			$fecha->salon          = Str::title(Input::get('salon'));
			$fecha->fecha_evento   = Input::get('fecha_evento');
			$fecha->hora_evento    = Input::get('hora_evento');
			$fecha->fecha_termina  = Input::get('fecha_termina');
			$fecha->hora_termina   = Input::get('hora_termina');
			$fecha->save();

			$total = 0;
			foreach (Input::get('nombre') as $key => $value) {
				$articulo = New PresupuestoArticulos;
				$articulo->id_presupuesto = $ID;
				$articulo->id_articulo    = $_POST['id_articulo'][$key];
				$articulo->nombre         = $_POST['nombre'][$key];
				$articulo->cantidad       = $_POST['cantidad'][$key];
				$articulo->precio         = $_POST['precio'][$key];
				$articulo->importe        = $_POST['importe'][$key];
				$articulo->save();

				$total    = $total + $_POST['importe'][$key];
			}

			$importe = Presupuesto::find($ID);
			$importe->precio = $total;
			$importe->save();

			return Redirect::to('presupuestos/create')->with('success',true);
		}
	}

	/**
	 * Display the specified resource.
	 * GET /presupuestos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$query = Presupuesto::find($id);

		$view = View::make('presupuestos.show')->with('query',$query);

		$this->layout->title   = "Presupuesto | Salón la Cúpula";
		$this->layout->menu    = 2;
		$this->layout->content = $view;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /presupuestos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$presupuesto = Presupuesto::find($id);
		$clientes   = array(""=>"&mdash; Seleccione Opción &mdash;") + Cliente::orderBy('nombre','asc')->lists('nombre','id');
		$paquetes   = array(""=>"&mdash; Seleccione Opción &mdash;") + Paquete::orderBy('clave','asc')->lists('clave','id');
		$categorias = array(""=>"&mdash; Seleccione Opción &mdash;") + Categoria::orderBy('nombre','asc')->lists('nombre','id');
		$view = View::make('presupuestos.edit')
					->with('categorias',$categorias)
					->with('paquetes',$paquetes)
					->with('clientes',$clientes)
					->with('presupuesto',$presupuesto);

		$this->layout->title   = "Presupuesto | Salón la Cúpula";
		$this->layout->menu    = 2;
		$this->layout->content = $view;
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /presupuestos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$data = array(
					'salon'         => Input::get('salon'),
					'fecha_evento'  => Input::get('fecha_evento'),
					'hora_evento'   => Input::get('hora_evento'),
					'fecha_termina' => Input::get('fecha_termina'),
					'hora_termina'  => Input::get('hora_termina')
					);
		$rules = array(
					'salon'         => 'required',
					'fecha_evento'  => 'required',
					'hora_evento'   => 'required',
					'fecha_termina' => 'required',
					'hora_termina'  => 'required'
					);

		$messages = array(
						'salon.required'         =>'El campo :attribute es requerido.',
						'fecha_evento.required'  =>'El campo Fecha Evento es requerido.',
						'hora_evento.required'   =>'El campo Hora Evento es requerido.',
						'fecha_termina.required' =>'El campo Fecha Termina es requerido.',
						'hora_termina.required'  =>'El campo Hora Termina es requerido.'
						);

		$validator = Validator::make($data,$rules,$messages);

		if ($validator->fails()) {

			return Redirect::to('presupuestos/'.$id.'/edit')->withErrors($validator)->withInput();
		}else{

			$fecha = PresupuestoFechas::where('id_presupuesto','=',$id)->first();
			$fecha->salon          = Str::title(Input::get('salon'));
			$fecha->fecha_evento   = Input::get('fecha_evento');
			$fecha->hora_evento    = Input::get('hora_evento');
			$fecha->hora_termina   = Input::get('hora_termina');
			$fecha->save();

			$total = 0;
			foreach (Input::get('nombre') as $key => $value) {

				$idArt = $_POST['id_articulo'][$key];

				$articulo = PresupuestoArticulos::find($idArt);
				$articulo->cantidad = $_POST['cantidad'][$key];
				$articulo->precio   = $_POST['precio'][$key];
				$articulo->importe  = $_POST['importe'][$key];
				$articulo->save();

				$total = $total + $_POST['importe'][$key];
			}

			$presupuesto = Presupuesto::find($id);
			$presupuesto->precio    = $total;
			$presupuesto->posted_by = Session::get('username');
			$presupuesto->save();

			return Redirect::to('presupuestos/'.$id.'/edit')->with('success',true);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /presupuestos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function listaArticulos()
	{
		$articulos = Articulos::where('id_paquete','=',Input::get('id'))->get();
		return View::make('presupuestos.lista')->with('articulos',$articulos);
	}

	public function contrato($id)
	{
		$contrato = Presupuesto::find($id);
		$view = View::make('presupuestos.contrato')
					->with('contrato',$contrato);

		$this->layout->title   = "Contrato | Salón la Cúpula";
		$this->layout->menu    = 2;
		$this->layout->content = $view;
	}

	public function cambiarFechas()
	{
		$presupuesto = PresupuestoFechas::where('id_presupuesto','=',Input::get('id'))->first();

		return View::make('presupuestos.cambiarFechas')->with('presupuesto',$presupuesto);

	}

	public function imprimirPresupuesto($id)
	{
		$query = Presupuesto::find($id);
		$view  = View::make('presupuestos.imprimir_presupuesto')->with('query',$query);

		return PDF::load($view, 'A4', 'portrait')->show(trim($query->cliente)."-".$query->paquete);

	}

	public function imprimirContrato($id,$dias)
	{
		$contrato = Presupuesto::find($id);
		$view = View::make('presupuestos.imprimir_contrato')
					->with('contrato',$contrato)
					->with('dias',$dias);

		return PDF::load($view,'A4','portrait')->show(trim($contrato->cliente)."-".$contrato->paquete);
	}

	public function putCambios()
	{
		$id = Input::get('id');

		$presupuesto = PresupuestoFechas::where('id_presupuesto','=',$id)->first();

		$presupuesto->fecha_evento = Input::get('fecha_evento');
		$presupuesto->hora_evento  = Input::get('hora_evento');
		$presupuesto->hora_termina = Input::get('hora_termina');
		$presupuesto->save();

		$query = Presupuesto::find($id);

		return View::make('presupuestos.cambiosFechas')->with('presupuesto',$query);

	}

	public function putCancelar()
	{
		$id = Input::get('idCancelar');

		$presupuesto = Presupuesto::find($id);
		$presupuesto->comentarios = Input::get('comentarios');
		$presupuesto->is_active   = 3;
		$presupuesto->save();

		$query = Presupuesto::find($id);

		return View::make('presupuestos.cambiosFechas')->with('presupuesto',$query);
	}

	public function aceptar()
	{
		$id = Input::get('id');

		$presupuesto = Presupuesto::find($id);
		$presupuesto->is_active = 2;
		$presupuesto->save();
		$respueta = "ok";
		return $respueta;
	}

	public function eventos()
	{
		$eventos = Presupuesto::whereIn('is_active',array('2','4'))->get();
		$data = array();

		foreach ($eventos as $evento) {
			$data[] = array(
				'title'=> $evento->paquete.' '.$evento->fechas->salon,
				'start'=> $evento->fechas->fecha_evento.'T'.$evento->fechas->hora_evento
				);
		}
		return json_encode($data);
	}

	public function eliminarArticulo()
	{
		$articulo = PresupuestoArticulos::find(Input::get('id'));
		$articulo->delete();
		return "ok";
	}
}
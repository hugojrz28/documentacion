<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Presupuesto </title>
    <link href='http://fonts.googleapis.com/css?family=Norican' rel='stylesheet' type='text/css'>
    <style>
    body{
        font-family: verdana,arial,sans-serif;
        font-size: 12px;
    }
    .text-cursive{
        font-family: 'Norican', cursive;
    }
    .invoice-name{
        font-size: 38px;
        text-align:center;
    }
    .sub-invoice-name{
        font-size:18px;
        text-align:center;
    }
    .slogan{
        font-size: 12px;
        font-weight: 300;
    }
    table {
        font-family: verdana,arial,sans-serif;
        font-size:11px;
        color:#333333;
        border-width: 1px;
        border-color: #999999;
        border-collapse: collapse;
        width: 100%;
    }
    table th {
        /*background-color:#c3dde0;*/
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #cccccc;
        text-align: left;
    }
    table tr {
        /*background-color:#d4e3e5;*/
    }
    table td {
        border-width: 1px;
        padding: 8px;
        border-style: solid;
        border-color: #cccccc;
    }
    </style>
</head>
    <body>
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-content">
                        <div class="invoice-info">
                            <div class="text-cursive invoice-name">
                                Salón la Cúpula<br>
                                <span class="sub-invoice-name">
                                    Salón de Eventos
                                </span>
                            </div>
                            <br>
                            <div class="invoice-from">
                                <span>De:</span>
                                <strong>Salón la Cúpula</strong>
                                <address>
                                    Periodistas No. 1404 Colonia Álamos
                                    <br>
                                    <abbr title="Phone">Tel:</abbr> 878-782-9718
                                    <br>
                                </address>
                            </div>
                            <br>
                            <div class="invoice-to">
                                <span>Para:</span>
                                <strong>{{$query->clientes->nombre}}</strong>
                                <address>
                                    {{$query->clientes->direccion}}
                                    <br>
                                    <abbr title="Phone">Tel:</abbr> {{$query->clientes->telefono}}
                                    <br>
                                </address>
                            </div>
                            <br>
                            <div class="invoice-infos" >
                                <table width="100%" border="0">
                                    <tbody>
                                        <tr>
                                            <td>Folio :</td>
                                            <td>{{$query->folio}}</td>
                                            <td>Paquete:</td>
                                            <td>{{$query->paquete}}</td>
                                            <td>Fecha Evento:</td>
                                            <td>{{$query->fechas->fecha_evento}} {{$query->fechas->hora_evento}}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <table width="100%" border="0">
                            <thead>
                                <tr>
                                    <th width="10%" align="left">No.</th>
                                    <th width="60%" align="left">Artículos</th>
                                    <th width="20%">Cantidad</th>
                                    <th width="10%">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $cont = 1; ?>
                                @foreach($query->items as $articulo)
                                <tr>

                                    <td>{{$cont;}}</td>
                                    <td>{{Str::title($articulo->nombre)}}</td>
                                    <td style="text-align:center;">{{number_format($articulo->cantidad)}}</td>
                                    <td>&nbsp;</td>
                                    <?php $cont++; ?>
                                </tr>
                                @endforeach
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td style="text-align:right;">Total</td>
                                    <td style="text-align:right;">${{number_format($query->precio,2)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <p style="font-size:12px;">
                    <span class="text-cursive slogan">Donde el buen gusto y la elegancia están a su alcance</span> <br>
                    Ing. H. Gilberto de los Reyes Castillo <br>
                    Tel: 78 2 97 18    &nbsp; &nbsp; &nbsp;  www.salondeeventoslacupula.com <br>
                    Estos precios no incluyen I.V.A. <br>
                    Mejoramos cualquier presupuesto. Se aplican restricciones
                </p>
            </div>
        </div>
    </body>
</html>
<div class="container-fluid">
    <!-- BEGIN HEADER TITLE -->
    <div class="row">
        <div class="col-sm-6">
            <div class="pull-left">
                <h1>Registro de presupuesto</h1>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="text-right" style="padding-top:5%;">
                <a href="{{URL::to('clientes/create')}}" class="btn btn-success">Añadir Cliente</a>
                <a href="{{URL::to('presupuestos')}}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Regresar</a>
            </div>
        </div>
    </div>
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="#">Inicio</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Presupuestos</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
	<br />
	@if (Session::has('success'))
	    <div class="alert alert-success fade in">
	        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4>Exito!</h4>
	        <p>Su presupuesto se registro de forma correcta!</p>
	    </div>
	@endif

	@if ($errors->has())
	    <div class="alert alert-danger fade in">
	        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4>Error!</h4>
	        @foreach ($errors->all('<p>:message</p>') as $message)
	            {{ $message }}
	        @endforeach
	    </div>
	@endif

	<div class="row" >
	        <div class="col-sm-12">
	            <div class="box box-bordered box-color">
	                <div class="box-title">
	                    <h3><i class="fa fa-th-list"></i>Registrar presupuestos</h3>
	                </div>
	                <div class="box-content">
	                	{{Form::open(array('url'=>'presupuestos','role'=>'form','class'=>'form-vertical','id'=>'frmPresupuesto'))}}
	                	<div class="row">
	                		<div class="col-sm-6">
	                			<div class="form-group">
	                				{{ Form::label('idCliente','Nombre Cliente') }}
	                        		{{ Form::select('idCliente',$clientes,Input::old('idCliente'),array('class'=>'form-control')) }}
	                			</div>
	                		</div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    {{Form::label('nombrePaquete','Nombre del Paquete')}}
                                    {{Form::text('nombrePaquete',Input::old('nombrePaquete'),array('class'=>'form-control'))}}
                                </div>
                            </div>
	                		<div class="col-sm-3">
	                			<div class="form-group">
	                				{{ Form::label('idPaquete','Elegir Paquete') }}
	                        		{{ Form::select('idPaquete',$paquetes,Input::old('idPaquete'),array('class'=>'form-control','id'=>'paquete')) }}
	                			</div>
	                		</div>
	                	</div>
	                	<div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    {{Form::label('salon','Salon')}}
                                    {{Form::text('salon',Input::old('salon'),array('class'=>'form-control'))}}
                                </div>
                            </div>
	                		<div class="col-sm-2">
	                			<div class="form-group">
	                				{{ Form::label('fecha_evento','Fecha Evento') }}
	                        		{{ Form::text('fecha_evento',Input::old('fecha_evento'),array('class'=>'datepicker form-control')) }}
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="form-group">
	                				{{ Form::label('hora_evento','Hora Evento') }}
	                        		{{ Form::text('hora_evento',Input::old('hora_evento'),array('class'=>'timepicker form-control')) }}
	                			</div>
	                		</div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    {{ Form::label('fecha_termina','Fecha Termina') }}
                                    {{ Form::text('fecha_termina',Input::old('fecha_termina'),array('class'=>'datepicker form-control')) }}
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    {{ Form::label('hora_termina','Hora Termina') }}
                                    {{ Form::text('hora_termina',Input::old('hora_termina'),array('class'=>'timepicker form-control')) }}
                                </div>
                            </div>
	                		<div class="col-sm-2">
	                			<div class="form-group">
	                				{{ Form::label('&nbsp;') }} <br>
	                        		<a id="btnAgenda" href="#" class="btn btn-info"><i class="fa fa-calendar"></i> Ver Agenda Eventos </a>
	                			</div>
	                		</div>
	                	</div>
	                    {{Form::label('Platillos / Consumibles')}}
	                    <div class="well">
	                    <div class="row">
	                    	<div class="col-sm-3">
	                        	<div class="form-group">
	                        		{{ Form::label('Categoría') }}
	                        		{{ Form::select('',$categorias,Input::old('categoria'),array('class'=>'form-control','id'=>'categoria')) }}
	                        	</div>
	                        </div>
	                        <div class="col-sm-6">
	                            <div id="content-articulos"  class="form-group">
	                                {{ Form::label('Articulo') }}
									{{ Form::select('',array(''=>'&mdash; Seleccione Opción &mdash;'),Input::old('articulo'),array('class'=>'form-control','id'=>'articulo')) }}
	                            </div>
	                        </div>
	                        <div class="col-sm-2">
	                        	<div class="form-group">
	                        		{{ Form::label('Cantidad') }}
	                        		{{ Form::text('',Input::old('Cantidad'),array('class'=>'form-control','id'=>'cantidad')) }}
	                        	</div>
	                        </div>
	                        <div class="col-sm-1">
	                        	<div class="form-group">
	                        		{{ Form::label('&nbsp;') }} <br>
	                        		<a id="btnAgregar" href="#" class="btn btn-info"><i class="fa fa-plus"></i> Agregar </a>
	                        	</div>
	                        </div>
	                    </div><!-- #END ROW -->
	                    </div>
						{{Form::label('Paquete / Artículos')}}
	                    <div class="row">
	                    	<div class="col-sm-12">
	                    		<table id="lista-articulos" class="table table-hover table-nomargin table-bordered">
		                            <thead>
		                                <tr>
		                                    <th>Nombre</th>
		                                    <th width="20%">Cantidad</th>
		                                    <th width="20%">Precio</th>
		                                    <th width="20%">Importe</th>
		                                    <th width="10%" class="text-center">Opciones</th>
		                                </tr>
		                            </thead>
		                            <tbody>

		                            </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" class="text-right"><a href="#" id="btnCalcular" class="btn btn-info">Total</a></td>
                                            <td> <input type="text" id="total" value="" class="form-control"> </td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
		                        </table>
	                    	</div>
	                    </div>
	                    <div class="form-actions pull-right">
	                        <input name="Restablecer" type="reset" class="btn" value="Limpiar">
                            <button id="btnSubmit" class="btn btn-success">Guardar</button>
	                    </div>
	                    {{ Form::close() }}
	                </div>
	            </div>
	        </div>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agenda Eventos</h4>
            </div>
            <div class="modal-body">
                <div id="calendar">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                {{-- <button type="button" class="btn btn-warning">Guardar cambios</button> --}}
            </div>
        </div>
    </div>
</div>

<script>
	$(document).on('ready',main);
	function main () {

		$('#categoria').on('change',cambiarArticulo);
		$('#btnAgregar').on('click',agregarArticulo);
		$('table tbody').on('click','a.eliminar',eliminarArticulo);
		$('#paquete').on('change',listaArticulos);
		$('#btnAgenda').on('click',showAgenda);
		$('#btnCalcular').on('click',calcularTotal2);
		$('.datepicker').datepicker({dateFormat:'yy-mm-dd'});
		$('.timepicker').timepicker({
                showInputs: false,
				showSeconds: false,
                showMeridian: false,
                defaultTime: false,
                minuteStep: 1
            });
        getEventos();
        // $('#btnSubmit').on('click', validar);
        $('#lista-articulos').on('change','.cantidad, .precio', calcular);
        $('#frmPresupuesto').on('keypress', function(e){
            var code = (e.keyCode ? e.keyCode : e.which);

            if (code == 13)
            {
                return false;
            }
        });
    }

    function calcular () {
        var ele = $(this).parent().parent();

        can = ele.find('.cantidad').val();
        pre = ele.find('.precio').val();
        tot = can * pre;

        ele.find('.importe').val(tot);
        calcularTotal();
    }

    function validar () {
        var frm = $('#frmPresupuesto');

        frm.validate({
            errorClass: "text-danger",
            rules:{
                'idCliente'    :{required:true},
                'idPaquete'    :{required:true},
                'salon'        :{required:true},
                'fecha_evento' :{required:true},
                'hora_evento'  :{required:true},
                'fecha_termina':{required:true},
                'hora_termina' :{required:true}
            },
            messages:{
                'idCliente'    :{required:"Campo requerido."},
                'idPaquete'    :{required:"Campo requerido."},
                'salon'        :{required:"Campo requerido."},
                'fecha_evento' :{required:"Campo requerido."},
                'hora_evento'  :{required:"Campo requerido."},
                'fecha_termina':{required:"Campo requerido."},
                'hora_termina' :{required:"Campo requerido."}
            },
            submitHandler: function(){
                frm.submit();
            }
        });
    }

    function getEventos () {
        $.ajax({
            url:"{{URL::to('calendario/eventos')}}",
            type: 'get',
            dataType:'json',
            success:function(eventos){
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    defaultDate: "{{date('Y-m-d')}}",
                    events: eventos ,
                    timeFormat: 'h:mm'
                });
            }
        });
    }

	function showAgenda () {
		$('#myModal').modal('show');
		return false;
	}

	function listaArticulos () {
		var paquete = $(this).val(), content = $('table tbody');
		$.ajax({
			'url' : './listaArticulos',
			'type': 'post',
			'data': {id:paquete},
			success:function(data){
				content.empty().append(data);
                calcularTotal();
			}
		});

		return false;
	}

    function calcularTotal () {
        var importe_total = 0
        $(".importe").each(function(index, value) {
                valor = $(this).val();
                importe_total += parseFloat(valor);
        });

        $("#total").val(parseFloat(importe_total).toFixed(2));
    }

    function calcularTotal2 () {
        var importe_total = 0
        $(".importe").each(function(index, value) {
                valor = $(this).val();
                importe_total += parseFloat(valor);
        });

        $("#total").val(parseFloat(importe_total).toFixed(2));

        return false;
    }

	function cambiarArticulo () {
		var categoria = $(this).val(), content = $('#content-articulos');

		$.ajax({
			'url' : '../paquetes/cambiarArticulo',
			'type': 'post',
			'data': {categoria:categoria},
			success:function(data){
				content.empty().append(data);
			}
		});

		return false;
	}

	function agregarArticulo () {
		var categoria = $('#categoria').val(), articulo = $('#articulo').val(), cantidad = $('#cantidad').val();

		$.ajax({
			'url' : '../paquetes/agregarArticuloDos',
			'type': 'post',
			'data': {cat:categoria,art:articulo,can:cantidad},
			success:function(data){
				$('table tbody').append(data);
                calcularTotal();
			}
		});
		return false;
	}

	function eliminarArticulo () {
		var articulo = $(this).parent().parent();

		articulo.css('background-color','#DD6D65').fadeOut(500).remove();
        calcularTotal();

		return false;
	}
</script>
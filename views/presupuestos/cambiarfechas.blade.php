<div class="form-group">
    {{Form::label('Fecha Evento')}}
    {{Form::text('fecha_evento',Input::old('fecha_evento',$presupuesto->fecha_evento),array('class'=>'datepicker form-control'))}}
</div>
<div class="form-group">
    {{Form::label('Hora Evento')}}
    {{Form::text('hora_evento',Input::old('hora_evento',$presupuesto->hora_evento),array('class'=>'timepicker form-control'))}}
</div>
<div class="form-group">
    {{Form::label('Hora Termina')}}
    {{Form::text('hora_termina',Input::old('hora_termina',$presupuesto->hora_termina),array('class'=>'timepicker form-control'))}}
</div>
{{Form::hidden('id',$presupuesto->id_presupuesto,array('id'=>'id'))}}
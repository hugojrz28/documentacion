<link href='http://fonts.googleapis.com/css?family=Norican' rel='stylesheet' type='text/css'>
<style>
    .invoice-name{
        font-family: 'Norican', cursive;
        font-size:38px;
        text-align:center;
    }
    .sub-invoice-name{
        font-family: 'Norican', cursive;
        font-size:18px;
        text-align:center;
    }
</style>
<div class="container-fluid">
    <!-- BEGIN HEADER TITLE -->
    <div class="row">
        <div class="col-sm-6">
            <div class="pull-left">
                <h1>Registro de presupuesto</h1>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="text-right" style="padding-top:5%;">
                <a href="{{URL::to('presupuestos')}}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Regresar</a>
            </div>
        </div>
    </div>
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="#">Inicio</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Presupuestos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
            	<a href="#">Detalle</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
	<hr />

    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-content">
                    <div class="invoice-info">
                        <div class="invoice-name">
                           Salón la Cúpula
                           <br>
                           <span class="sub-invoice-name"> Salón de Eventos</span>
                        </div>

                        <div class="invoice-from">
                            <span>De</span>
                            <strong>Salón la Cúpula</strong>
                            <address>
                                Periodistas No. 1404 Colonia Álamos
                                <br>
                                <br>
                                <abbr title="Phone">Tel:</abbr> 878-782-9718
                                <br>
                            </address>
                        </div>
                        <div class="invoice-to">
                            <span>Para</span>
                            <strong>{{$query->clientes->nombre}}</strong>
                            <address>
                                {{$query->clientes->direccion}}
                                <br>
                                <br>
                                <abbr title="Phone">Tel:</abbr> {{$query->clientes->telefono}}
                                <br>
                            </address>
                        </div>
                        <div class="invoice-infos">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Fecha:</th>
                                        <td>{{date_format($query->created_at,"d/m/Y H:i:s")}}</td>
                                    </tr>
                                    <tr>
                                        <th>Folio :</th>
                                        <td>{{$query->folio}}</td>
                                    </tr>
                                    <tr>
                                        <th>Paquete:</th>
                                        <td>{{$query->paquete}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <table class="table table-striped table-invoice">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="50%">Articulo</th>
                                {{-- <th>Precio</th> --}}
                                <th width="20%">Cantidad</th>
                                {{-- <th class="text-right">Total</th> --}}
                                <th width="20%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $cont = 1; ?>
                            @foreach($query->items as $articulo)
                            <tr>
                                <?php $total = ($articulo->precio*$articulo->cantidad); ?>
                                <td>{{$cont;}}</td>
                                <td class="name">{{Str::title($articulo->nombre)}}</td>
                                {{-- <td class="price">{{number_format($articulo->precio,2)}}</td> --}}
                                <td class="qty">{{number_format($articulo->cantidad)}}</td>
                                {{-- <td class="total">{{number_format($total,2)}}</td> --}}
                                <td></td>
                                <?php $cont++;?>
                            </tr>
                            @endforeach

                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td></td>
                                <td class="taxes">
                                    <p>
                                        <span class="light">Total</span>
                                        <span class="totalprice">
                                            ${{number_format($query->precio,2)}}
                                        </span>
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-right">
            <a href="{{URL::to('presupuestos/imprimir/'.$query->id)}}" target="_blank" class="btn btn-success">Imprimir presupuesto</a>
        </div>
    </div>

</div>
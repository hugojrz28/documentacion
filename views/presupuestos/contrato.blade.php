<link href='http://fonts.googleapis.com/css?family=Norican' rel='stylesheet' type='text/css'>
<style>
    .invoice-name{
        font-family: 'Norican', cursive;
        font-size:38px;
        text-align:center;
    }
    .sub-invoice-name{
        font-family: 'Norican', cursive;
        font-size:18px;
        text-align:center;
    }
</style>
<div class="container-fluid">
    <!-- BEGIN HEADER TITLE -->
    <div class="row">
        <div class="col-sm-6">
            <div class="pull-left">
                <h1>Registro de presupuesto</h1>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="text-right" style="padding-top:5%;">
                <a href="{{URL::to('presupuestos')}}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Regresar</a>
            </div>
        </div>
    </div>

    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="#">Inicio</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Presupuestos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Contrato</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-content">
                    <div class="invoice-info">
                        <div class="invoice-name">
                           Salón la Cúpula
                           <br>
                           <span class="sub-invoice-name"> Salón de Eventos</span>
                        </div>
                    </div>
                    <p>
                        Contrato que se celebra entre el Señor Hector Gilberto de los Reyes Castillo, en su carácter de propietario del salón
                        denominado la Cúpula, por parte; y por la otra, el(la) Sr.(a) <strong>{{$contrato->cliente}}</strong> a los que en lo sucesivo, respectivamente;
                        Prestador de Servicios, y por la otra El Cliente, respectivamente, ambos, mayores de edad, con capacidad legal para
                        contratar y obligarse válidamente; y manifiestan que es su voluntad obligarse a los términos;
                    </p>
                    <div class="text-center">
                        <h4>CLAUSULAS</h4>
                    </div>
                    <p>
                        PRIMERA:
                    </p>
                    <p>
                        Manifiesta el prestador ser legitimo propietario de un inmueble ubicado en la calle Periodistas No. 1404 de la
                        Colonia Álamos, el cual se encuentra acondicionado para eventos sociales, y que dicho local reúne todas las condiciones
                         de higiene y salubridad correspondientes.
                    </p>
                    <p>
                        SEGUNDA:
                    </p>
                    <p>
                        Manifiesta el cliente <strong>{{$contrato->cliente}}</strong> que es su deseo contratar los servicios de renta el salón y sus accesorios de acuerdo al paquete: <strong>{{$contrato->paquete}}</strong> con fecha el <strong>{{$contrato->fechas->fecha_evento}}</strong> según anexo uno
                    </p>
                    <p>
                        TERCERA:
                    </p>
                    <p>
                        Manifiesto el prestador Hector Gilberto de los Reyes Castillo que se obliga a proporcionar todos los servicios que se
                        describen en el anexo uno de este contrato, en el horario comprendido entre las: <strong>{{$contrato->fechas->hora_evento}}</strong> <strong>{{$contrato->fechas->fecha_evento}}</strong> Y <strong>{{$contrato->fechas->hora_termina}}</strong> <strong>{{$contrato->fechas->fecha_termina}}</strong> y a la vez se supervisara el buen funcionamiento de los servicios que preste, y a respetar la fecha del evento en los términos que se contrato.
                    </p>
                    <p>
                        CUARTA:
                    </p>
                    <p>
                        Por su parte El Cliente se obliga ademas
                        <ul>
                            <li>A) A servirse del Salón de Eventos y a conservar el local en buen estado en que lo recibió, y a devolverlo en el mismo estado al terminar el contrato</li>
                            <li>B) Recibir los alimentos en el horario previamente fijado</li>
                            <li>C) A pagar los daños que causen sus invitados el día del evento o fiesta</li>
                            <li>D) A entregar un 30% como anticipo del monto total del evento</li>
                            <li>E) A pagar el total del precio del evento <input type="text" id="dias" name="dias" style="width:50px; display:inline;" class="form-control" value="5"> días antes de su celebración</li>
                            <li>F) A perder el derecho de la devolución de los anticipos por la cancelación del evento</li>
                            <li>G) En los 15 días próximos al evento si se llegara a cambiar la fecha se cobrara un 30% del costo del salón</li>
                            <li>H) No pegar artículos en la pared con cintas adhesivas o clavos se cobrara daños materiales</li>
                            <li>I) En el caso del mal tiempo como por ejemplo lluvia no hay devolución en la renta de los juegos inflables</li>
                            <li>J) Así mismo el prestador no se hace responsable de ningún accidente que ocurra dentro y fuera del salón por ejemplo alguna caída, golpe, cortada, etc.</li>
                        </ul>
                    </p>
                    <p>
                        QUINTA:
                    </p>
                    <p>
                        Manifiestan AMBAS PARTES que saben y están conscientes, que en caso que las autoridades competentes ordenen cerrar el salón por algún siniestro ajeno a la voluntad de ellas; ambos se obligan a concretar una nueva fecha primeramente señalada. Así mismo que el estacionamiento no cuenta con vigilancia policíaca. Leído que fue por ambas partes; manifestaron que no existe error, dolo, violencia, ningún otra causa que invalide o nulifique lo aquí contratado por ser expresa su voluntad
                    </p>
                    <p>
                        SEXTA:
                    </p>
                    <p>
                        En caso de algún siniestro, tormenta, lluvia, choque, etc, que cause la falta de energía eléctrica de la ciudad el SALON LA CUPULA no se hace responsable.
                    </p>
                    <p>
                        EL Cliente <strong>{{$contrato->cliente}}</strong>
                        <address>
                            Dirección: {{$contrato->clientes->direccion}}
                            <br>
                            <abbr title="Phone">Tel:</abbr>{{$contrato->clientes->telefono}}
                            <br>
                        </address>
                    </p>


                </div>
            </div>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-sm-6">___________________________________</div>
        <div class="col-sm-6">___________________________________</div>
    </div>
    <div class="row text-center">
        <div class="col-sm-6">FIRMA PRESTADOR DE SERVICIOS</div>
        <div class="col-sm-6">FIRMA CLIENTE</div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="alert">
                <strong>NOTA:</strong>
                <u>No se permite introducir juegos inflables u otros objetos que puedan dañar EL SALON LA CUPULA</u>
                <br />
                <br />
                <p>
                    Donde el buen gusto y la elegancia están a su alcance
                </p>
                <p>
                    Ing. H. Gilberto de los Reyes Castillo
                </p>
                <p>
                    Tel: 878-782-9718
                </p>
                <p>
                    www.salondeeventoslacupula.com
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div id="msj" class="col-sm-10 text-right">
            @if ($contrato->is_active == 2)
                <span class="text-success"><i class="fa fa-check"></i> Contrato Aceptado</span>
            @elseif($contrato->is_active == 3)
                <span class="text-danger"><i class="fa fa-remove"></i> Contrato Cancelado</span>
            @else
                <a href="#" id="btnAceptar" data-id="{{$contrato->id}}" class="btn btn-success">Aceptar Contrato</a>
            @endif
        </div>
        <div class="col-sm-2 text-right">
            <a id="btnImprimir" href="#" target="_blank" class="btn btn-warning">Imprimir Contrato</a>
        </div>
    </div>

</div>

<script>
    $(document).on('ready',main);
    function main () {
        $('#btnAceptar').on('click',aceptarContrato);
        $('#btnImprimir').on('click',imprimirContrato);
    }

    function aceptarContrato () {
        var id = $(this).data('id');

        $.ajax({
            url :"{{URL::to('contrato/aceptar')}}",
            type:'post',
            data:{id:id},
            success: function(response){
                $('#msj').empty().append("<span class='text-success'><i class='fa fa-check'></i> Contrato Aceptado</span>");
            }
        });
        return false;
    }

    function imprimirContrato () {
        var target = "{{URL::to('contrato/imprimir/'.$contrato->id)}}", dias = $("#dias").val();

        $(this).attr('href',target+'/'+dias);
    }
</script>
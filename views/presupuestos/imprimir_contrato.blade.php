<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Contrato</title>
    <link href='http://fonts.googleapis.com/css?family=Norican' rel='stylesheet' type='text/css'>
    <style>
    body{
        font-family: verdana,arial,sans-serif;
        font-size: 13px;
    }
    .text-cursive{
        font-family: 'Norican', cursive;
    }
    .invoice-name{
        font-size: 36px;
        text-align:center;
    }
    .sub-invoice-name{
        font-size:18px;
        text-align:center;
    }
    .slogan{
        font-size: 13px;
        font-weight: 300;
    }
    </style>
</head>
<body>
<div class="content-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-content" style="margin-top:0px;">
                    <div class="invoice-info">
                        <div class="text-cursive invoice-name">
                            Salón la Cúpula<br>
                            <span class="sub-invoice-name">
                                Salón de Eventos
                            </span>
                        </div>
                    </div>
                    <p>
                        Contrato que se celebra entre el Señor Hector Gilberto de los Reyes Castillo, en su carácter de propietario del salón
                        denominado la Cúpula, por parte; y por la otra, el(la) Sr.(a) <strong>{{$contrato->cliente}}</strong> a los que en lo sucesivo, respectivamente;
                        Prestador de Servicios, y por la otra El Cliente, respectivamente, ambos, mayores de edad, con capacidad legal para
                        contratar y obligarse válidamente; y manifiestan que es su voluntad obligarse a los términos;
                    </p>
                    <div style="text-align:center;">
                        <strong>CLAUSULAS</strong>
                    </div>
                    <p>
                        PRIMERA: <br>
                        Manifiesta el prestador ser legitimo propietario de un inmueble ubicado en la calle Periodistas No. 1404 de la
                        Colonia Álamos, el cual se encuentra acondicionado para eventos sociales, y que dicho local reúne todas las condiciones
                         de higiene y salubridad correspondientes.
                        <br>
                        SEGUNDA: <br>
                        Manifiesta el cliente <strong>{{$contrato->cliente}}</strong> que es su deseo contratar los servicios de renta el salón y sus accesorios de acuerdo al paquete: <strong>{{$contrato->paquete}}</strong> con fecha el <strong>{{$contrato->fechas->fecha_evento}}</strong> según anexo uno
                        <br>
                        TERCERA: <br>
                        Manifiesto el prestador Hector Gilberto de los Reyes Castillo que se obliga a proporcionar todos los servicios que se
                        describen en el anexo uno de este contrato, en el horario comprendido entre las: <strong>{{$contrato->fechas->hora_evento}}</strong> <strong>{{$contrato->fechas->fecha_evento}}</strong> Y <strong>{{$contrato->fechas->hora_termina}}</strong> <strong>{{$contrato->fechas->fecha_termina}}</strong> y a la vez se supervisara el buen funcionamiento de los servicios que preste, y a respetar la fecha del evento en los términos que se contrato.
                        <br>
                        CUARTA: <br>
                        Por su parte El Cliente se obliga ademas
                        <ul>
                            <li>A) A servirse del Salón de Eventos y a conservar el local en buen estado en que lo recibió, y a devolverlo en el mismo estado al terminar el contrato</li>
                            <li>B) Recibir los alimentos en el horario previamente fijado</li>
                            <li>C) A pagar los daños que causen sus invitados el día del evento o fiesta</li>
                            <li>D) A entregar un 30% como anticipo del monto total del evento</li>
                            <li>E) A pagar el total del precio del evento {{ $dias }} días antes de su celebración</li>
                            <li>F) A perder el derecho de la devolución de los anticipos por la cancelación del evento</li>
                            <li>G) En los 15 días próximos al evento si se llegara a cambiar la fecha se cobrara un 30% del costo del salón</li>
                            <li>H) No pegar artículos en la pared con cintas adhesivas o clavos se cobrara daños materiales</li>
                            <li>I) En el caso del mal tiempo como por ejemplo lluvia no hay devolución en la renta de los juegos inflables</li>
                            <li>J) Así mismo el prestador no se hace responsable de ningún accidente que ocurra dentro y fuera del salón por ejemplo alguna caída, golpe, cortada, etc.</li>
                        </ul> <br>
                        QUINTA: <br>
                        Manifiestan AMBAS PARTES que saben y están conscientes, que en caso que las autoridades competentes ordenen cerrar el salón por algún siniestro ajeno a la voluntad de ellas; ambos se obligan a concretar una nueva fecha primeramente señalada. Así mismo que el estacionamiento no cuenta con vigilancia policíaca. Leído que fue por ambas partes; manifestaron que no existe error, dolo, violencia, ningún otra causa que invalide o nulifique lo aquí contratado por ser expresa su voluntad. <br>
                        SEXTA: <br>
                        En caso de algún siniestro, tormenta, lluvia, choque, etc, que cause la falta de energía eléctrica de la ciudad el SALON LA CUPULA no se hace responsable. <br>
                        EL cliente <strong>{{$contrato->cliente}}</strong> Dirección: {{$contrato->clientes->direccion}} <abbr title="Phone">Tel:</abbr> {{$contrato->clientes->telefono}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="row text-center">
        <table width="100%">
            <tr>
                <td style="text-align:center;">___________________________________</td>
                <td style="text-align:center;">___________________________________</td>
            </tr>
            <tr>
                <td style="text-align:center;">FIRMA PRESTADOR DE SERVICIOS</td>
                <td style="text-align:center;">FIRMA CLIENTE</td>
            </tr>
        </table>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="alert">
                <strong>NOTA:</strong>
                <u>No se permite introducir juegos inflables u otros objetos que puedan dañar EL SALON LA CUPULA</u>
                <p style="font-size:12px;">
                    <span class="text-cursive slogan">Donde el buen gusto y la elegancia están a su alcance</span> <br>
                    Ing. H. Gilberto de los Reyes Castillo <br>
                    Tel: 78 2 97 18
                    www.salondeeventoslacupula.com
                </p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
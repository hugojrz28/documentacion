@foreach($articulos as $articulo)
<tr>
	<td>{{$articulo->nombre}}
		{{Form::hidden('id_articulo[]',$articulo->id)}}
		{{Form::hidden('nombre[]',$articulo->nombre)}}
	</td>
	<td>
		{{Form::text('cantidad[]',$articulo->cantidad,array('class'=>'cantidad form-control'))}}
	</td>
	<td>
		{{Form::hidden('precio[]',$articulo->precio,array('class'=>'precio form-control'))}}
	</td>
	<td>
		{{Form::hidden('importe[]',$articulo->importe,array('class'=>'importe form-control'))}}
	</td>
	<td class="text-center">
		<a href="#" data-id="{{$articulo->id}}" class="eliminar btn btn-danger"><i class="fa fa-trash"></i></a>
	</td>
</tr>
@endforeach

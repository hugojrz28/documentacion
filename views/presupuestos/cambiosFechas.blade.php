<td>
  <?php
  switch ($presupuesto->is_active) {
    case '1':
    echo "<span class='label label-warning'>Por Confirmar</span>";
    break;
    case '2':
    echo "<span class='label label-success'>Confirmado</span>";
    break;
    case '3':
    echo "<span class='label label-danger'>Cancelado</span>";
    break;
    case '4':
    echo "<span class='label label-info'>Pagado</span>";
    break;
  }
  ?>
</td>
<td>{{$presupuesto->folio}}</td>
<td>{{$presupuesto->paquete}}</td>
<td>{{$presupuesto->cliente}}</td>
<td>{{$presupuesto->fechas->fecha_evento}}</td>
<td>{{$presupuesto->fechas->hora_evento}}</td>
<td>{{$presupuesto->fechas->hora_termina}}</td>
<td class="text-center">
  <a href="{{URL::to('presupuestos/'.$presupuesto->id)}}" class="btn btn-default" title="Ver presupuesto"><i class="fa fa-search"></i></a>
  <a href="{{URL::to('presupuestos/contrato/'.$presupuesto->id)}}" class="btn btn-success" title="Ver contrato"><i class="fa fa-file-text"></i></a>
  <a href="#" data-id="{{$presupuesto->id}}" class="editar btn btn-info" title="Cambiar fecha evento" data-toggle="modal" data-target="#myModal"><i class="fa fa-calendar"></i> </a>
  <a href="{{URL::to('presupuestos/'.$presupuesto->id.'/edit')}}" class="btn btn-warning" title="Editar presupuesto"><i class="fa fa-pencil"></i></a>
  <a href="#" data-id="{{$presupuesto->id}}" class="cancelar btn btn-danger" data-toggle="modal" data-target="#myModalCancelar" title="Cancelar presupuesto" ><i class="fa fa-trash"></i></a>
</td>
<div class="container-fluid">
    <!-- BEGIN HEADER TITLE -->
    <div class="row">
        <div class="col-sm-6">
            <div>
                <h1>Registro de presupuestos</h1>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="text-right" style="padding-top:5%;">
                <a href="{{URL::to('presupuestos/create')}}" class="btn btn-success">Añadir Presupuesto</a>
            </div>
        </div>
    </div>
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="#">Inicio</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Presupuesto</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <!-- TABLA CLIENTES -->
    <div class="row">
            <div class="col-sm-12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3>Presupuestos registrados</h3>
                    </div>
                    <div class="box-content nopadding">
                        <table id="reporte1" class="table table-hover table-nomargin table-bordered dataTable">
                            <thead>
                                <tr>
                                	<th>Status</th>
                                    <th>Folio</th>
                                    <th>Paquete</th>
                                    <th>Nombre Cliente</th>
                                    <th>Fecha Evento</th>
                                    <th>Hora Evento</th>
                                    <th>Hora Termina</th>
                                    <th class="text-center">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($presupuestos) > 0)
                                    @foreach($presupuestos as $presupuesto)
                                        <tr id="registro-{{$presupuesto->id}}">
                                        	<td>
                                                <?php
                                                    switch ($presupuesto->is_active) {
                                                      case '1':
                                                        echo "<span class='label label-warning'>Por Confirmar</span>";
                                                        break;
                                                      case '2':
                                                        echo "<span class='label label-success'>Confirmado</span>";
                                                        break;
                                                      case '3':
                                                        echo "<span class='label label-danger'>Cancelado</span>";
                                                        break;
                                                      case '4':
                                                        echo "<span class='label label-info'>Pagado</span>";
                                                        break;
                                                    }
                                                ?>
                                            </td>
                                            <td>{{$presupuesto->folio}}</td>
                                            <td>{{$presupuesto->paquete}}</td>
                                            <td>{{$presupuesto->cliente}}</td>
                                            <td>{{$presupuesto->fechas->fecha_evento}}</td>
                                            <td>{{$presupuesto->fechas->hora_evento}}</td>
                                            <td>{{$presupuesto->fechas->hora_termina}}</td>
                                            <td class="text-center">
                                                <a href="{{URL::to('presupuestos/'.$presupuesto->id)}}" class="btn btn-default" title="Ver presupuesto"><i class="fa fa-search"></i></a>
                                                <a href="{{URL::to('presupuestos/contrato/'.$presupuesto->id)}}" class="btn btn-success" title="Ver contrato"><i class="fa fa-file-text"></i></a>
                                                <a href="#" data-id="{{$presupuesto->id}}" class="editar btn btn-info" title="Cambiar fecha evento" data-toggle="modal" data-target="#myModal"><i class="fa fa-calendar"></i> </a>
                                                <a href="{{URL::to('presupuestos/'.$presupuesto->id.'/edit')}}" class="btn btn-warning" title="Editar presupuesto"><i class="fa fa-pencil"></i></a>
                                                <a href="#" data-id="{{$presupuesto->id}}" class="cancelar btn btn-danger" data-toggle="modal" data-target="#myModalCancelar" title="Cancelar presupuesto" ><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center"> No hay presupuestos registrados</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>

</div> <!-- #END CONTAINER-FLUID -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cambiar Fecha Evento</h4>
            </div>
            <div class="modal-body">
                <form id="frmFecha">
                    <div class="form-group">
                        {{Form::label('Fecha Evento')}}
                        {{Form::text('fecha_evento',null,array('class'=>'form-control'))}}
                    </div>
                    <div class="form-group">
                        {{Form::label('Hora Evento')}}
                        {{Form::text('hora_evento',null,array('class'=>'form-control'))}}
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnCambiar" class="btn btn-warning">Guardar cambios</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalCancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Cancelar Evento</h4>
            </div>
            <div class="modal-body">
                <form id="frmCancelar">
                    <div class="form-group">
                        {{ Form::label('Comentarios') }}
                        {{ Form::textarea('comentarios', Input::old('comentarios'), array('class'=>'form-control','rows'=>'3') ) }}
                        <input type="hidden" name="idCancelar" id="idCancelar" value="">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnCancelar" class="btn btn-danger">Guardar cambios</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('ready', main);
    function main () {
        $('.editar').on('click',cambiarFechas);
        $('.cancelar').on('click',cancelarEvento);
        $('#frmFecha').on('focusin','.datepicker',function(){
            $(this).datepicker({dateFormat:'yy-mm-dd'});
        });
        $('#frmFecha').on('focusin','.timepicker',function(){
            $(this).timepicker({
                showMeridian: false,
                defaultTime: false
            });
        });
        $('#btnCambiar').on('click',putCambios);
        $('#btnCancelar').on('click',putCancelar);
    }

    function putCancelar () {
        var data = $('#frmCancelar').serialize(), id = $('#idCancelar').val();

        $.ajax({
            'url':'./presupuestos/putCancelar',
            'type':'post',
            'data':data,
            success: function(response){
                $('#myModalCancelar').modal('hide');
                $('#registro-'+id).empty().append(response);
            }
        });
        return false;
    }

    function putCambios () {
        var data = $('#frmFecha').serialize(), id = $("#id").val();
        console.log(id);
        $.ajax({
            'url' :'./presupuestos/putCambios',
            'type':'post',
            'data': data,
            success: function(response){
                $("#myModal").modal('hide');
                $("#registro-"+id).empty().append(response);
            }
        });
        return false;
    }

    function cancelarEvento () {
        var id = $(this).data('id');
        $("#idCancelar").val(id);
        $('#myModalCancelar').modal('show');
        return false;
    }

    function cambiarFechas () {
        var id = $(this).data('id');

        $.ajax({
            'url' :'./presupuestos/cambiarFechas',
            'type':'post',
            'data':{id:id},
            success:function(data){
                $('#myModal').modal('show');
                $('#frmFecha').empty().append(data);
            }
        });

        return false;
    }
</script>